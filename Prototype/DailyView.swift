//
//  DailyView.swift
//  Prototype
//
//  Created by Steven Jo on 25/06/19.
//  Copyright © 2019 steven.siswanto. All rights reserved.
//

import UIKit

class DailyView: UIView {

    @IBOutlet var treeView: UIImageView!
    @IBOutlet var circleView: UIImageView!
    @IBOutlet var characterView: UIImageView!
    @IBOutlet var otherView: UIImageView!
    @IBOutlet var confettiView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupParallax()
    }

    func setupParallax() {
        addMotion(toView: treeView, value: 20)
        addMotion(toView: circleView, value: 20)
        addMotion(toView: characterView, value: 40)
        addMotion(toView: otherView, value: 40)
        addMotion(toView: confettiView, value: 60)
    }
    
    func addMotion(toView: UIView, value: CGFloat) {
        let min = CGFloat(-value)
        let max = CGFloat(value)
        
        let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = min
        xMotion.maximumRelativeValue = max
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = min
        yMotion.maximumRelativeValue = max
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion,yMotion]
        
        toView.addMotionEffect(motionEffectGroup)
    }
    
    @IBAction func removeMotion() {
        subviews.forEach { (s) in
            s.motionEffects.forEach({ s.removeMotionEffect($0) })
        }
    }
}
